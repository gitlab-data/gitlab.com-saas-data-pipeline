from postgres_pyspark_utils import (
    get_spark_session,
    get_gcs_connection,
    check_table_exist,
    get_table_list,
)
from yaml import safe_load, YAMLError
from pyspark.sql import SQLContext

# Create spark session for iceberg catalog
spark = get_spark_session()
sql_context = SQLContext(spark)
database_name = "gitlab_data_postgres_test"

table_list = get_table_list()
raw_file_path = "staging/backfill/"
staging_bucket = "gs://test-saas-pipeline-backfills/"


def get_latest_file_staging(table: str):
    bucket_source = get_gcs_connection("test-saas-pipeline-backfills")
    blobs = bucket_source.list_blobs(prefix=f"{raw_file_path}{table}")
    parquet_files = []
    for blob in blobs:
        if blob.name.endswith(".parquet.gzip"):
            parquet_files.append(blob.name)
    return max(parquet_files, default=None)


conf = spark.sparkContext._jsc.hadoopConfiguration()

conf.set("fs.gs.impl", "com.google.cloud.hadoop.fs.gcs.GoogleHadoopFileSystem")
conf.set(
    "fs.AbstractFileSystem.gs.impl", "com.google.cloud.hadoop.fs.gcs.GoogleHadoopFS"
)

for table in table_list:
    if not check_table_exist(spark, database_name, table):
        latest_parquet_file = get_latest_file_staging(table)
        if latest_parquet_file:
            print(latest_parquet_file)
            parDF = sql_context.read.parquet(f"{staging_bucket}{latest_parquet_file}")
            print(parDF.dtypes)
            column_list = []
            for i, (column_name, data_type) in enumerate(parDF.dtypes):
                column_name_d = column_name + " " + data_type
                column_list.append(column_name_d)
            table_column_data_type = ",".join(column_list)
            table_ddl = f"CREATE TABLE {database_name}.{table} ( {table_column_data_type}  ) USING ICEBERG;"
            print(table_ddl)
            spark.sql(table_ddl).show()
            spark.sql(f"select * from {database_name}.{table};").show()
