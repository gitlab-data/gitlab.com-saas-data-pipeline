# Import necessary libraries
from postgres_pyspark_utils import get_spark_session


# create the database
database_name = "gitlab_data_postgres_test"

# Create spark session for iceberg catalog
spark = get_spark_session("PROD")

spark.sql(f"CREATE DATABASE IF NOT EXISTS {database_name}")

print(spark.catalog.listDatabases())

spark.stop()
