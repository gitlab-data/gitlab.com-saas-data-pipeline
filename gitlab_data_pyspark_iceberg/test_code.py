from pyspark.sql import SparkSession
from pyspark.sql.functions import col, explode, from_json, json_tuple, get_json_object
from pyspark.sql.types import (
    StructType,
    StructField,
    StringType,
    ArrayType,
    NullType,
    TimestampType,
    LongType,
    MapType,
)


# Create a SparkSession
spark = SparkSession.builder.appName("Explode JSON").getOrCreate()


schema = StructType(
    [
        StructField("schema", StringType(), False),
        StructField(
            "data",
            ArrayType(
                StructType(
                    [
                        StructField("schema", StringType(), False),
                        StructField(
                            "data",
                            StructType(
                                [
                                    StructField("environment", StringType(), True),
                                    StructField("source", StringType(), True),
                                    StructField("plan", NullType(), True),
                                    StructField(
                                        "extra",
                                        StructType(
                                            [
                                                StructField("id", LongType(), True),
                                                StructField(
                                                    "merge_request_id", LongType(), True
                                                ),
                                                StructField(
                                                    "latest_build_started_at",
                                                    TimestampType(),
                                                    True,
                                                ),
                                                StructField(
                                                    "latest_build_finished_at",
                                                    TimestampType(),
                                                    True,
                                                ),
                                                StructField(
                                                    "first_deployed_to_production_at",
                                                    TimestampType(),
                                                    True,
                                                ),
                                                StructField(
                                                    "merged_at", TimestampType(), True
                                                ),
                                                StructField(
                                                    "created_at", StringType(), True
                                                ),
                                                StructField(
                                                    "updated_at", StringType(), True
                                                ),
                                                StructField(
                                                    "pipeline_id", StringType(), True
                                                ),
                                                StructField(
                                                    "merged_by_id", StringType(), True
                                                ),
                                                StructField(
                                                    "latest_closed_by_id",
                                                    StringType(),
                                                    True,
                                                ),
                                                StructField(
                                                    "latest_closed_at",
                                                    StringType(),
                                                    True,
                                                ),
                                                StructField(
                                                    "first_comment_at",
                                                    StringType(),
                                                    True,
                                                ),
                                                StructField(
                                                    "first_commit_at",
                                                    StringType(),
                                                    True,
                                                ),
                                                StructField(
                                                    "last_commit_at", StringType(), True
                                                ),
                                                StructField(
                                                    "diff_size", StringType(), True
                                                ),
                                                StructField(
                                                    "modified_paths_size",
                                                    StringType(),
                                                    True,
                                                ),
                                                StructField(
                                                    "commits_count", StringType(), True
                                                ),
                                                StructField(
                                                    "first_approved_at",
                                                    StringType(),
                                                    True,
                                                ),
                                                StructField(
                                                    "first_reassigned_at",
                                                    StringType(),
                                                    True,
                                                ),
                                                StructField(
                                                    "added_lines", StringType(), True
                                                ),
                                                StructField(
                                                    "removed_lines", StringType(), True
                                                ),
                                            ]
                                        ),
                                        True,
                                    ),
                                    StructField("user_id", StringType(), True),
                                    StructField("namespace_id", StringType(), True),
                                    StructField("project_id", StringType(), True),
                                    StructField(
                                        "context_generated_at", StringType(), True
                                    ),
                                ]
                            ),
                            False,
                        ),
                    ]
                ),
                False,
            ),
        ),
    ]
)

parDF = (
    spark.read.option("delimiter", "\t")
    .csv("/Users/vedprakash/Downloads/sample_events.txt")
    .select(
        col("_c0").alias("APP_ID"),
        col("_c1").alias("PLATFORM"),
        col("_c2").alias("ETL_TSTAMP"),
        col("_c3").alias("COLLECTOR_TSTAMP"),
        col("_c4").alias("DVCE_CREATED_TSTAMP"),
        col("_c52").alias("CONTEXT"),
        col("_c53").alias("SE_CATEGORY"),
        col("_c54").alias("SE_ACTION"),
        col("_c55").alias("SE_LABEL"),
        col("_c56").alias("EVENT_TYPE"),
    )
)


df_merge_request_metrics = parDF.select(
    "APP_ID",
    "PLATFORM",
    "ETL_TSTAMP",
    "COLLECTOR_TSTAMP",
    "DVCE_CREATED_TSTAMP",
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.id").alias("id"),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.merge_request_id").alias(
        "merge_request_id"
    ),
    get_json_object(
        col("CONTEXT"), "$.data[0].data.extra.latest_build_started_at"
    ).alias("latest_build_started_at"),
    get_json_object(
        col("CONTEXT"), "$.data[0].data.extra.latest_build_finished_at"
    ).alias("latest_build_finished_at"),
    get_json_object(
        col("CONTEXT"), "$.data[0].data.extra.first_deployed_to_production_at"
    ).alias("first_deployed_to_production_at"),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.merged_at").alias(
        "merged_at"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.created_at").alias(
        "created_at"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.updated_at").alias(
        "updated_at"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.pipeline_id").alias(
        "pipeline_id"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.merged_by_id").alias(
        "merged_by_id"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.latest_closed_by_id").alias(
        "latest_closed_by_id"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.latest_closed_at").alias(
        "latest_closed_at"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.first_comment_at").alias(
        "first_comment_at"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.first_commit_at").alias(
        "first_commit_at"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.last_commit_at").alias(
        "last_commit_at"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.diff_size").alias(
        "diff_size"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.modified_paths_size").alias(
        "modified_paths_size"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.commits_count").alias(
        "commits_count"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.first_approved_at").alias(
        "first_approved_at"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.first_reassigned_at").alias(
        "first_reassigned_at"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.added_lines").alias(
        "added_lines"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.removed_lines").alias(
        "removed_lines"
    ),
    "SE_CATEGORY",
    "SE_ACTION",
    "SE_LABEL",
    "EVENT_TYPE",
)
df_vulnerabilities = parDF.select(
    "APP_ID",
    "PLATFORM",
    "ETL_TSTAMP",
    "COLLECTOR_TSTAMP",
    "DVCE_CREATED_TSTAMP",
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.id").alias("id"),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.confidence").alias(
        "confidence"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.confidence_overridden").alias(
        "confidence_overridden"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.confirmed_at").alias(
        "confirmed_at"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.dismissed_at").alias(
        "dismissed_at"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.resolved_at").alias(
        "resolved_at"
    ),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.state").alias("state"),
    get_json_object(col("CONTEXT"), "$.data[0].data.extra.updated_at").alias(
        "updated_at"
    ),
    "SE_CATEGORY",
    "SE_ACTION",
    "SE_LABEL",
    "EVENT_TYPE",
)
df_merge_request_metrics.show(1, truncate=False)
df_vulnerabilities.show(1, truncate=False)

import os
from slack import WebClient

slack_token = os.environ["SLACK_API_TOKEN"]
client = WebClient(token=slack_token)

response = client.chat_postEphemeral(
    channel="T02592416", text="Hello silently from your app! :tada:", user="U0XXXXXXX"
)
