from datetime import datetime, timedelta

import pandas as pd


def get_date_interval_list(start_date_range: str, end_date_range: str) -> list:
    """Generate time interval range with end_date of one interval overlapping the other, and extract value from
    dataframe and generate list of interval."""
    date_list = pd.DataFrame(
        {
            "Date": pd.interval_range(
                start_date_range, end_date_range, periods=10, closed="neither"
            )
        }
    )
    convert_interval_to_list = []
    for _, row in date_list.iterrows():
        convert_interval_to_list.append(str(row._get_value("Date")))
    return convert_interval_to_list


def get_start_end_date_value(list_date_interval):
    split_date_list = list_date_interval.split(sep=",")
    start_date_striped = split_date_list[0].replace("(", "").replace(")", "").strip()
    end_date_striped = split_date_list[1].replace("(", "").replace(")", "").strip()
    return start_date_striped, end_date_striped


def get_datetime_format(date_value):
    if len(date_value) > length_start_date:
        return (
            datetime.strptime(
                date_value[: -(len(date_value) - length_start_date)],
                "%Y-%m-%d %H:%M:%S",
            )
        ).strftime("%Y-%m-%dT%H:%M:%S")

    return (datetime.strptime(date_value, "%Y-%m-%d %H:%M:%S")).strftime(
        "%Y-%m-%dT%H:%M:%S"
    )


if __name__ == "__main__":
    end_date = pd.to_datetime(datetime.now().strftime("%Y-%m-%dT%H:%M:%S"))
    start_date = "2016-07-25T01:00:00"
    start_date = pd.to_datetime(start_date)
    # Get length of fixed column for stripping extra nanoseconds downstream.
    length_start_date = len(str(start_date))
    final_date_list = []
    date_interval_list = get_date_interval_list(start_date, end_date)

    for date_interval_list in date_interval_list:
        start_date_value, end_date_value = get_start_end_date_value(date_interval_list)
        date_range_dict = {
            "start_date": get_datetime_format(start_date_value),
            "end_date": get_datetime_format(end_date_value),
        }
        final_date_list.append(date_range_dict)
    print(final_date_list)
