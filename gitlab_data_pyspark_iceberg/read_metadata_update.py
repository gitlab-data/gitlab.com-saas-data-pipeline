from pyspark.sql import SparkSession, SQLContext
from google.cloud.storage import Client

spark = (
    SparkSession.builder.appName("Load_to_iceberg")
    .config("spark.jars.packages", "org.apache.iceberg:iceberg-spark3-runtime:0.12.1")
    .config(
        "spark.sql.extensions",
        "org.apache.iceberg.spark.extensions.IcebergSparkSessionExtensions",
    )
    .config(
        "spark.sql.catalog.spark_catalog",
        "org.apache.iceberg.spark.SparkSessionCatalog",
    )
    .config("spark.sql.catalog.spark_catalog.type", "hive")
    .config("spark.sql.catalog.local", "org.apache.iceberg.spark.SparkCatalog")
    .config("spark.sql.catalog.local.type", "hadoop")
    .config("spark.sql.catalog.local.warehouse", "$PWD/warehouse")
    .config("spark.sql.catalogImplementation", "hive")
    .config("spark.sql.legacy.createHiveTableByDefault", False)
    .config("spark.sql.warehouse.dir", "gs://sds-dataproc-production/spark-warehouse")
    .getOrCreate()
)

sql_context = SQLContext(spark)


df = spark.read.format("iceberg").load(
    "gitlab_data_postgres_test.merge_request_metrics"
)
df.createOrReplaceTempView("merge_request_metrics")


spark.sql(
    """select count(1) from gitlab_data_postgres_test.merge_request_metrics"""
).show()
spark.sql(
    """select * from gitlab_data_postgres_test.merge_request_metrics limit 10"""
).show()
spark.sql(
    """UPDATE  gitlab_data_postgres_test.merge_request_metrics set first_commit_at = "Ved Prakash commit";"""
)
spark.sql(
    """select * from gitlab_data_postgres_test.merge_request_metrics limit 10"""
).show()
# spark.sql(query)
