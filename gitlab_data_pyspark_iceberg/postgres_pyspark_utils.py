from pyspark.sql import SparkSession
from google.cloud.storage import Client
from yaml import safe_load

# Create a SparkSession

# Create a SparkSession
def get_spark_session(environment_type: str = None):
    if environment_type == "PROD":
        spark = (
            SparkSession.builder.appName("Parquet to Iceberg")
            .config(
                "spark.jars.packages",
                "org.apache.iceberg:iceberg-spark-runtime-3.2_2.12:1.1.0",
            )
            .config(
                "spark.sql.extensions",
                "org.apache.iceberg.spark.extensions.IcebergSparkSessionExtensions",
            )
            .config(
                "spark.sql.catalog.iceberg_prod_catalog",
                "org.apache.iceberg.spark.SparkSessionCatalog",
            )
            .config("spark.sql.catalog.iceberg_prod_catalog.type", "hive")
            .config(
                "spark.sql.catalog.iceberg_prod_catalog.uri",
                "thrift://10.138.0.38:9080",
            )
            .getOrCreate()
        )
    else:
        spark = (
            SparkSession.builder.appName("Load_to_iceberg")
            .config(
                "spark.jars.packages",
                "org.apache.iceberg:iceberg-spark-runtime-3.2_2.12:1.1.0",
            )
            .config(
                "spark.sql.extensions",
                "org.apache.iceberg.spark.extensions.IcebergSparkSessionExtensions",
            )
            .config(
                "spark.sql.catalog.spark_catalog",
                "org.apache.iceberg.spark.SparkSessionCatalog",
            )
            .config("spark.sql.catalog.spark_catalog.type", "hive")
            .config("spark.sql.catalog.local", "org.apache.iceberg.spark.SparkCatalog")
            .config("spark.sql.catalog.local.type", "hadoop")
            .config("spark.sql.catalog.local.warehouse", "$PWD/warehouse")
            .config("spark.sql.catalogImplementation", "hive")
            .config("spark.sql.legacy.createHiveTableByDefault", False)
            .config(
                "spark.sql.warehouse.dir",
                "gs://sds-dataproc-production/spark-warehouse",
            )
            .getOrCreate()
        )

    return spark


#


def get_gcs_connection(bucket_name):
    client = Client()
    bucket = client.get_bucket(bucket_name)
    return bucket


def get_table_list() -> list:
    bucket = get_gcs_connection("sds-dataproc-production")
    blob = bucket.get_blob(
        "gitlab_data_pyspark_iceberg/saas_gitlab_com_table_manifest.yaml"
    )
    downloaded_file = blob.download_as_text(encoding="utf-8")
    stream = safe_load(downloaded_file)
    return list(stream["tables"])


def check_table_exist(spark, database_name: str, table_name: str):
    """Check if the specified table exist in the database or not."""
    table_list = spark.sql(f"show tables in {database_name}")
    table_name = table_list.filter(table_list.tableName == f"{table_name}").collect()
    if len(table_name) > 0:
        print(f"{table_name} table found")
        return True
    print(f"{table_name} table not found")
    return False
