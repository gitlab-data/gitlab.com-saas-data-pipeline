from postgres_pyspark_utils import get_spark_session, check_table_exist
from pyspark.sql import SQLContext

# Create spark session for iceberg catalog
spark = get_spark_session()
sql_context = SQLContext(spark)
database_name = "gitlab_data_postgres_test"


def create_gitlab_db_merge_request_metric_scd():
    if not check_table_exist(
        spark, database_name, "gitlab_db_merge_request_metric_scd"
    ):
        merge_request_metric_scd_table_ddl = f"CREATE TABLE {database_name}.gitlab_db_merge_request_metric_scd ( \
                                        ID bigint,\
                                        MERGE_REQUEST_ID bigint, \
    	                                LATEST_BUILD_STARTED_AT string, \
    	                                LATEST_BUILD_FINISHED_AT string,\
    	                                FIRST_DEPLOYED_TO_PRODUCTION_AT string, \
    	                                MERGED_AT string, \
    	                                CREATED_AT string, \
    	                                UPDATED_AT string, \
    	                                PIPELINE_ID bigint, \
    	                                MERGED_BY_ID bigint, \
    	                                LATEST_CLOSED_BY_ID bigint, \
    	                                LATEST_CLOSED_AT timestamp, \
    	                                FIRST_COMMENT_AT timestamp, \
    	                                FIRST_COMMIT_AT timestamp, \
    	                                LAST_COMMIT_AT timestamp, \
    	                                DIFF_SIZE bigint, \
    	                                MODIFIED_PATHS_SIZE bigint, \
    	                                COMMITS_COUNT bigint, \
    	                                FIRST_APPROVED_AT timestamp, \
    	                                FIRST_REASSIGNED_AT timestamp, \
    	                                ADDED_LINES bigint, \
    	                                REMOVED_LINES bigint, \
    	                                _UPLOADED_AT FLOAT, \
                                        START_DATE_TS DATE, \
                                        END_DATE_TS DATE, \
                                        IS_ACTIVE boolean, \
                                        Is_DELETED boolean) USING ICEBERG;"
        spark.sql(merge_request_metric_scd_table_ddl).show()

def create_gitlab_db_merge_request_metric_snowplow():
    if not check_table_exist(
        spark, database_name, "gitlab_db_merge_request_metric_snowplow"
    ):
        gitlab_db_merge_request_metric_snowplow_table_ddl = f"CREATE TABLE {database_name}.gitlab_db_merge_request_metric_snowplow ( \
                                        event_created_at_ts timestamp,\
                                        ID bigint,\
                                        MERGE_REQUEST_ID bigint, \
    	                                LATEST_BUILD_STARTED_AT string, \
    	                                LATEST_BUILD_FINISHED_AT string,\
    	                                FIRST_DEPLOYED_TO_PRODUCTION_AT string, \
    	                                MERGED_AT string, \
    	                                CREATED_AT string, \
    	                                UPDATED_AT string, \
    	                                PIPELINE_ID bigint, \
    	                                MERGED_BY_ID bigint, \
    	                                LATEST_CLOSED_BY_ID bigint, \
    	                                LATEST_CLOSED_AT timestamp, \
    	                                FIRST_COMMENT_AT timestamp, \
    	                                FIRST_COMMIT_AT timestamp, \
    	                                LAST_COMMIT_AT timestamp, \
    	                                DIFF_SIZE bigint, \
    	                                MODIFIED_PATHS_SIZE bigint, \
    	                                COMMITS_COUNT bigint, \
    	                                FIRST_APPROVED_AT timestamp, \
    	                                FIRST_REASSIGNED_AT timestamp, \
    	                                ADDED_LINES bigint, \
    	                                REMOVED_LINES bigint, \
    	                                EVENT_TYPE string) USING ICEBERG;"
        spark.sql(gitlab_db_merge_request_metric_snowplow_table_ddl).show()



def create_gitlab_db_vulnerabilities_snowplow():
    if not check_table_exist(spark, database_name, "gitlab_db_vulnerabilities_snowplow"):
        gitlab_db_vulnerabilities_snowplow_table_ddl = f"CREATE TABLE {database_name}.gitlab_db_vulnerabilities_snowplow ( \
                                        event_created_at_ts timestamp, \
                                        ID bigint, \
    	                                CONFIDENCE bigint, \
    	                                CONFIDENCE_OVERRIDDEN BOOLEAN, \
    	                                CONFIRMED_AT timestamp, \
    	                                CREATED_AT timestamp, \
    	                                DISMISSED_AT timestamp, \
    	                                RESOLVED_AT timestamp, \
    	                                SEVERITY_OVERRIDDEN BOOLEAN, \
    	                                STATE bigint, \
    	                                UPDATED_AT timestamp, \
    	                                EVENT_TYPE string) USING ICEBERG;"
        spark.sql(gitlab_db_vulnerabilities_snowplow_table_ddl).show()

def create_gitlab_db_vulnerabilities_scd():
    if not check_table_exist(spark, database_name, "gitlab_db_vulnerabilities_scd"):
        gitlab_db_vulnerabilities_scd_table_ddl = f"CREATE TABLE {database_name}.gitlab_db_vulnerabilities_scd ( \
                                        ID bigint, \
    	                                CONFIDENCE bigint, \
    	                                CONFIDENCE_OVERRIDDEN BOOLEAN, \
    	                                CONFIRMED_AT timestamp, \
    	                                CREATED_AT timestamp, \
    	                                DISMISSED_AT timestamp, \
    	                                RESOLVED_AT timestamp, \
    	                                SEVERITY_OVERRIDDEN BOOLEAN, \
    	                                STATE bigint, \
    	                                UPDATED_AT timestamp, \
    	                                _UPLOADED_AT FLOAT, \
                                        START_DATE_TS DATE, \
                                        END_DATE_TS DATE, \
                                        IS_ACTIVE boolean, \
                                        Is_DELETED boolean) USING ICEBERG;"
        spark.sql(gitlab_db_vulnerabilities_scd_table_ddl).show()
        
        
create_gitlab_db_merge_request_metric_scd()
create_gitlab_db_vulnerabilities_scd()
create_gitlab_db_merge_request_metric_snowplow()
create_gitlab_db_vulnerabilities_snowplow()
