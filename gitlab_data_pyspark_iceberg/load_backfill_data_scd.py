"""
    This peace of code is responsible to get the list of table from yaml file and load all the backfill data into Iceberg table.
"""
from pyspark.sql import SQLContext
from yaml import YAMLError
from postgres_pyspark_utils import get_spark_session, get_gcs_connection, get_table_list


# get Spark session
spark = get_spark_session()
# embed it in sql context
sql_context = SQLContext(spark)

## COnfigure GCS bucket for file system integration to spark session
conf = spark.sparkContext._jsc.hadoopConfiguration()
conf.set("fs.gs.impl", "com.google.cloud.hadoop.fs.gcs.GoogleHadoopFileSystem")
conf.set(
    "fs.AbstractFileSystem.gs.impl", "com.google.cloud.hadoop.fs.gcs.GoogleHadoopFS"
)
raw_file_path = "staging/backfill/"
staging_bucket = "gs://test-saas-pipeline-backfills/"
bucket_source = get_gcs_connection("test-saas-pipeline-backfills")


def get_file_subdirectory(table_name):
    """Get the subdirectory of the filename to pass the to iceberg procedure"""
    blobs = bucket_source.list_blobs(prefix=f"{raw_file_path}{table_name}")
    parquet_files = []

    for blob in blobs:
        if blob.name.endswith(".parquet.gzip"):
            parquet_files.append(blob.name)

    last_file = max(parquet_files, default=None)
    print(last_file)
    if last_file:
        return last_file.split("/")[-2], parquet_files
    return None


"""
for file in parquet_files:
    df1=spark.read.parquet(f"{staging_bucket}{file}")
    df1.writeTo("gitlab_data_postgres_test.merge_request_metrics").append()
"""

table_list = get_table_list()

insert_gitlab_db_merge_request_metric_scd = "INSERT INTO gitlab_data_postgres_test.gitlab_db_merge_request_metric_scd SELECT ID ,MERGE_REQUEST_ID ,LATEST_BUILD_STARTED_AT ,LATEST_BUILD_FINISHED_AT ,FIRST_DEPLOYED_TO_PRODUCTION_AT ,MERGED_AT ,CREATED_AT ,UPDATED_AT ,PIPELINE_ID ,MERGED_BY_ID ,LATEST_CLOSED_BY_ID ,TO_TIMESTAMP(LATEST_CLOSED_AT, 'yyyy-MM-DD HH:m:s+SS') ,TO_TIMESTAMP(FIRST_COMMENT_AT, 'yyyy-MM-DD HH:m:s+SS')  ,TO_TIMESTAMP(FIRST_COMMIT_AT, 'yyyy-MM-DD HH:m:s+SS')  ,TO_TIMESTAMP(LAST_COMMIT_AT, 'yyyy-MM-DD HH:m:s+SS')  ,DIFF_SIZE ,MODIFIED_PATHS_SIZE ,COMMITS_COUNT ,TO_TIMESTAMP(FIRST_APPROVED_AT, 'yyyy-MM-DD HH:m:s+SS') ,TO_TIMESTAMP(FIRST_REASSIGNED_AT, 'yyyy-MM-DD HH:m:s+SS')  ,ADDED_LINES ,REMOVED_LINES , NULL AS _UPLOADED_AT ,  CURRENT_DATE() START_DATE_TS ,  to_date('2099-12-31', 'yyyy-MM-DD') END_DATE_TS ,  True IS_ACTIVE ,  False Is_DELETED from gitlab_data_postgres_test.merge_request_metrics;"
insert_gitlab_db_vulnerabilities_scd = "INSERT INTO gitlab_data_postgres_test.gitlab_db_vulnerabilities_scd SELECT ID, CONFIDENCE, CONFIDENCE_OVERRIDDEN, TO_TIMESTAMP(CONFIRMED_AT, 'yyyy-MM-DD HH:m:s+SS'), TO_TIMESTAMP(CREATED_AT, 'yyyy-MM-DD HH:m:s+SS') ,TO_TIMESTAMP(DISMISSED_AT, 'yyyy-MM-DD HH:m:s+SS') ,TO_TIMESTAMP(RESOLVED_AT, 'yyyy-MM-DD HH:m:s+SS') , SEVERITY_OVERRIDDEN, STATE, TO_TIMESTAMP(UPDATED_AT, 'yyyy-MM-DD HH:m:s+SS') , NULL AS _UPLOADED_AT ,  CURRENT_DATE() START_DATE_TS ,  to_date('2099-12-31', 'yyyy-MM-DD') END_DATE_TS ,  True IS_ACTIVE ,  False Is_DELETED from gitlab_data_postgres_test.vulnerabilities;"

# iterate and point the parquet file to iceberg table.
for table in table_list:

    sub_directory, parquet_files = get_file_subdirectory(table)

    if sub_directory:
        try:
            # spark.sql(
            #    f"""call spark_catalog.system.add_files(
            # table => 'gitlab_data_postgres_test.{table}',
            # source_table => '`parquet`.`gs://test-saas-pipeline-backfills/staging/backfill/{table}/{sub_directory}/`')
            # """
            # )
            for file in parquet_files:
                df1 = spark.read.parquet(f"{staging_bucket}{file}")
                df1.writeTo(f"gitlab_data_postgres_test.{table}").append()
        except Exception as e:
            print(e)
            continue

        spark.sql(
            f"""
        select count(1) from gitlab_data_postgres_test.{table}
        """
        ).show()


