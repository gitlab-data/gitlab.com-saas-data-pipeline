import os
from pyspark.sql import SQLContext
from pyspark.sql.functions import col, get_json_object
from pyspark.sql.types import (
    StructType,
    StructField,
    StringType,
    ArrayType,
    NullType,
    TimestampType,
    LongType,
    MapType,
)
from google.cloud.storage import Client
from yaml import safe_load, YAMLError
from postgres_pyspark_utils import get_gcs_connection, get_spark_session

staging_bucket = "gs://gitlab-db-snowplow/"
bucket_source = get_gcs_connection("gitlab-db-snowplow")
straming_file_path = "2023/07/01/"
# Create spark session for iceberg catalog
spark = get_spark_session()
sql_context = SQLContext(spark)
database_name = "gitlab_data_postgres_test"
conf = spark.sparkContext._jsc.hadoopConfiguration()
conf.set("fs.gs.impl", "com.google.cloud.hadoop.fs.gcs.GoogleHadoopFileSystem")
conf.set(
    "fs.AbstractFileSystem.gs.impl", "com.google.cloud.hadoop.fs.gcs.GoogleHadoopFS"
)


def get_file_subdirecotory():
    """Get the sub direcotry of the filename to pass the to iceberg procedure"""
    blobs = list(bucket_source.list_blobs(prefix=f"{straming_file_path}"))
    folders = sorted(list(set([os.path.dirname(k.name) for k in blobs])))
    folder_path = []
    for path in folders:
        folder_path.append(f"{staging_bucket}{path}")
    print(folder_path)
    return folder_path


list_of_raw_files_sub_folders = get_file_subdirecotory()
print(list_of_raw_files_sub_folders)
# str_of_raw_files_sub_folders=','.join([str(item) for item in list_of_raw_files_sub_folders ])
# read first file and check data types.
total_rows_merge_request_metrics = 0
total_rows_vulnerabilities = 0
"""
schema = StructType([
    StructField("schema", StringType(), False),
    StructField("data", ArrayType(
        StructType([
            StructField("schema", StringType(), False),
            StructField("data", StructType([
                StructField("environment", StringType(), True),
                StructField("source", StringType(), True),
                StructField("plan", NullType(), True),
                StructField("extra", StructType([
                    StructField("id", LongType(), True),
                    StructField("merge_request_id", LongType(), True),
                    StructField("latest_build_started_at", TimestampType(), True),
                    StructField("latest_build_finished_at", TimestampType(), True),
                    StructField("first_deployed_to_production_at", TimestampType(), True),
                    StructField("merged_at", TimestampType(), True),
                    StructField("created_at", StringType(), True),
                    StructField("updated_at", StringType(), True),
                    StructField("pipeline_id", StringType(), True),
                    StructField("merged_by_id", StringType(), True),
                    StructField("latest_closed_by_id", StringType(), True),
                    StructField("latest_closed_at", StringType(), True),
                    StructField("first_comment_at", StringType(), True),
                    StructField("first_commit_at", StringType(), True),
                    StructField("last_commit_at", StringType(), True),
                    StructField("diff_size", StringType(), True),
                    StructField("modified_paths_size", StringType(), True),
                    StructField("commits_count", StringType(), True),
                    StructField("first_approved_at", StringType(), True),
                    StructField("first_reassigned_at", StringType(), True),
                    StructField("added_lines", StringType(), True),
                    StructField("removed_lines", StringType(), True)
                ]), True),
                StructField("user_id", StringType(), True),
                StructField("namespace_id", StringType(), True),
                StructField("project_id", StringType(), True),
                StructField("context_generated_at", StringType(), True)
            ]), False)
        ]), False)
    )
])"""

def select_columns_scd_tables(table_name: str, column_name: str):
     df = sql_context.sql(f" select {column_name} from {database_name}.{table_name}")
     return df

target_dataframe_vulnerabilities=select_columns_scd_tables("gitlab_db_vulnerabilities_scd","id")
target_dataframe_merge_request_metric=select_columns_scd_tables("gitlab_db_merge_request_metric_scd","id")

print(target_dataframe_merge_request_metric.count())
print(target_dataframe_vulnerabilities.count())

for files in list_of_raw_files_sub_folders[:1]:
    parDF = (
        sql_context.read.option("delimiter", "\t")
        .csv(files)
        .select(
            col("_c0").alias("APP_ID"),
            col("_c1").alias("PLATFORM"),
            col("_c2").alias("ETL_TSTAMP"),
            col("_c3").alias("COLLECTOR_TSTAMP"),
            col("_c4").alias("DVCE_CREATED_TSTAMP"),
            col("_c52").alias("CONTEXT"),
            col("_c53").alias("SE_CATEGORY"),
            col("_c54").alias("SE_ACTION"),
            col("_c55").alias("SE_LABEL"),
            col("_c56").alias("EVENT_TYPE"),
        )
    )

    parDF_merge_request_metrics = parDF.filter(
        (col("SE_LABEL") == 'merge_request_metrics') & (col("APP_ID") == 'gitlab')
    )
    # parDF_merge_request_metrics_update=parDF.filter(col("SE_LABEL")=='merge_request_metrics' & col("APP_ID")=='gitlab' & col("EVENT_TYPE") == 'update')
    # parDF_merge_request_metrics_delete=parDF.filter(col("SE_LABEL")=='merge_request_metrics' & col("APP_ID")=='gitlab' & col("EVENT_TYPE") == 'delete')

    parDF_vulnerabilities = parDF.filter(
        (col("SE_LABEL") == "vulnerabilities") & (col("APP_ID") == "gitlab")
    )
    # parDF_vulnerabilities_update=parDF.filter(col("SE_LABEL")=='vulnerabilities' & col("APP_ID")=='gitlab' & col("EVENT_TYPE") == 'update')
    # parDF_vulnerabilities_delete=parDF.filter(col("SE_LABEL")=='vulnerabilities' & col("APP_ID")=='gitlab' & col("EVENT_TYPE") == 'delete')

    df_merge_request_metrics = parDF_merge_request_metrics.select(
        col("DVCE_CREATED_TSTAMP").cast("timestamp").alias("event_created_at_ts"),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.id").cast("bigint").alias("id"),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.merge_request_id").cast("bigint").alias(
            "merge_request_id"
        ),
        get_json_object(
            col("CONTEXT"), "$.data[0].data.extra.latest_build_started_at"
        ).cast("timestamp").alias("latest_build_started_at"),
        get_json_object(
            col("CONTEXT"), "$.data[0].data.extra.latest_build_finished_at"
        ).cast("timestamp").alias("latest_build_finished_at"),
        get_json_object(
            col("CONTEXT"), "$.data[0].data.extra.first_deployed_to_production_at"
        ).cast("timestamp").alias("first_deployed_to_production_at"),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.merged_at").cast("timestamp").alias(
            "merged_at"
        ),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.created_at").cast("timestamp").alias(
            "created_at"
        ),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.updated_at").cast("timestamp").alias(
            "updated_at"
        ),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.pipeline_id").cast("bigint").alias(
            "pipeline_id"
        ),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.merged_by_id").cast("bigint").alias(
            "merged_by_id"
        ),
        get_json_object(
            col("CONTEXT"), "$.data[0].data.extra.latest_closed_by_id"
        ).cast("bigint").alias("latest_closed_by_id"),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.latest_closed_at").cast("timestamp").alias(
            "latest_closed_at"
        ),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.first_comment_at").cast("timestamp").alias(
            "first_comment_at"
        ),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.first_commit_at").cast("timestamp").alias(
            "first_commit_at"
        ),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.last_commit_at").cast("timestamp").alias(
            "last_commit_at"
        ),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.diff_size").cast("bigint").alias(
            "diff_size"
        ),
        get_json_object(
            col("CONTEXT"), "$.data[0].data.extra.modified_paths_size"
        ).cast("bigint").alias("modified_paths_size"),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.commits_count").cast("bigint").alias(
            "commits_count"
        ),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.first_approved_at").cast("timestamp").alias(
            "first_approved_at"
        ),
        get_json_object(
            col("CONTEXT"), "$.data[0].data.extra.first_reassigned_at"
        ).cast("timestamp").alias("first_reassigned_at"),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.added_lines").cast("bigint").alias(
            "added_lines"
        ),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.removed_lines").cast("bigint").alias(
            "removed_lines"
        ),
        "EVENT_TYPE",
    )
    df_vulnerabilities = parDF_vulnerabilities.select(
        col("DVCE_CREATED_TSTAMP").cast("timestamp").alias("event_created_at_ts"),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.id").cast("bigint").alias("id"),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.confidence").cast("bigint").alias(
            "confidence"
        ),
        get_json_object(
            col("CONTEXT"), "$.data[0].data.extra.confidence_overridden"
        ).cast("boolean").alias("confidence_overridden"),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.confirmed_at").cast("timestamp").alias(
            "confirmed_at"
        ),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.severity_overridden").cast("boolean").alias(
            "severity_overridden"
        ),
         get_json_object(col("CONTEXT"), "$.data[0].data.extra.created_at").cast("timestamp").alias(
            "created_at"
        ),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.dismissed_at").cast("timestamp").alias(
            "dismissed_at"
        ),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.resolved_at").cast("timestamp").alias(
            "resolved_at"
        ),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.state").cast("bigint").alias("state"),
        get_json_object(col("CONTEXT"), "$.data[0].data.extra.updated_at").cast("timestamp").alias(
            "updated_at"
        ),
        "EVENT_TYPE",
    )
    
    df_merge_request_metrics.show(20, truncate=False)
    df_vulnerabilities.show(20, truncate=False)
    total_rows_merge_request_metrics += parDF_vulnerabilities.count()
    total_rows_vulnerabilities += parDF_vulnerabilities.count()
    df_merge_request_metrics.writeTo("gitlab_data_postgres_test.gitlab_db_merge_request_metric_snowplow").append()
    df_vulnerabilities.writeTo("gitlab_data_postgres_test.gitlab_db_vulnerabilities_snowplow").append()
    



print(f"total_rows_merge_request_metrics: {total_rows_merge_request_metrics}")
print(f"total_rows_vulnerabilities: {total_rows_vulnerabilities}")
