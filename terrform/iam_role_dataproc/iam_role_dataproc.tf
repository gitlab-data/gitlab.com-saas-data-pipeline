resource "google_project_iam_custom_role" "data_proc_admin_role" {
  role_id     = "dataprocadminrole"
  title       = "Data proc admin role"
  description = "This role will be resposible for all data proc related access and object creation"
  permissions = ["iam.roles.list", "iam.roles.create", "iam.roles.delete"]
}