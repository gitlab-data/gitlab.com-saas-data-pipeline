provider "google" {
    project = "gitlab-analysis"
    region  = "us-west1"
    zone    = "us-west1-a" 
}
variable "environment" {
    type = string
}

variable "network_mode" {
    type = string
}

variable "subnetwork" {
  type = string
}

variable "sds_dataproc_pool_node_count" {
    type = number
}

terraform {
  backend "gcs" {
    bucket  = "gitlab-analysis-data-terraform-state"
    prefix  = "dataproc_gke/state"
  }
}


resource "google_container_cluster" "sds_dataproc_primary_gke" {
    project = "gitlab-analysis"
    location = "us-west1-a"
    provider = google-beta
    name     = "sds-dataproc-${var.environment}"
    description = "${var.environment} Dataproc cluster for spark"
    network = var.network_mode
    remove_default_node_pool = true
    subnetwork=var.subnetwork
    initial_node_count       = 1
    addons_config {
      horizontal_pod_autoscaling {
        disabled=false
      }
    }
   release_channel {
        channel="UNSPECIFIED"
    }
    notification_config {
        pubsub {
            enabled = false
        }
    }
    vertical_pod_autoscaling {
        enabled=false
    }
    private_cluster_config {
        enable_private_endpoint = false
    }
    workload_identity_config {
    workload_pool = "gitlab-analysis.svc.id.goog"
}
}

resource "google_container_node_pool" "sds_dataproc_pool" {
    name        = "sds-dataproc-${var.environment}"
    location = "us-west1-a"
    cluster     = google_container_cluster.sds_dataproc_primary_gke.name
    node_count = var.sds_dataproc_pool_node_count
    
    node_config {
        machine_type    = "n2-standard-2"
        image_type = "COS_CONTAINERD"
        disk_type = "pd-standard"
        disk_size_gb = 100
        preemptible = false
    }
    upgrade_settings {
      max_surge=1
      max_unavailable=0
    }
    management {
      auto_repair=true
      auto_upgrade=true
    }
}