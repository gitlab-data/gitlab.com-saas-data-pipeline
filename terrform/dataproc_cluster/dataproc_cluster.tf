provider "google" {
    project = "gitlab-analysis"
    region  = "us-west1"
    zone    = "us-west1-a" 
}
variable "environment" {
    type = string
}

variable "network_mode" {
    type = string
}

variable "subnetwork" {
  type = string
}

variable "sds_dataproc_pool_node_count" {
    type = number
}

terraform {
  backend "gcs" {
    bucket  = "gitlab-analysis-data-terraform-state"
    prefix  = "sds_google_dataproc_gke_poc/state"
  }
}


resource "google_storage_bucket" "staging_bucket" {
  name          = "sds-dataproc-${var.environment}-gke"
  location      = "us-west1"
  force_destroy = true

  uniform_bucket_level_access = true
}

resource "google_dataproc_cluster" "dataproc_gke_cluster" {
  name     = "sds-dataproc-gke"
  region   = "us-west1"
  graceful_decommission_timeout = "120s"

  virtual_cluster_config {
    staging_bucket = google_storage_bucket.staging_bucket.name
    auxiliary_services_config {
        metastore_config {
          dataproc_metastore_service = "projects/gitlab-analysis/locations/us-west1/services/sds-data-ops"
        }
    }
    kubernetes_cluster_config {
        kubernetes_namespace = "iceberg"

        kubernetes_software_config {
          component_version = {
            "SPARK" : "3.1-dataproc-12"
          }

          properties = {
            "spark:spark.eventLog.enabled": "true",
            "spark:spark.jars.package":"org.apache.iceberg:iceberg-spark3-runtime:0.12.0"
          }
        }
        gke_cluster_config {
          gke_cluster_target = "projects/gitlab-analysis/locations/us-west1-a/clusters/sds-dataproc-production"
          node_pool_target {
            node_pool = "data-proc-poc"
            roles = ["DEFAULT"]
            
            node_pool_config {
              autoscaling {
                min_node_count = 1
                max_node_count = 3
              }
                
                config {
                    machine_type    = "n2-standard-2"
                    preemptible = false
                    
              }

              locations = ["us-west1-a"]
            }
        }
    }
  }
}
}