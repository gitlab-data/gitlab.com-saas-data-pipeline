provider "google" {
    project = "gitlab-analysis"
    region  = "us-west1"
    zone    = "us-west1-a" 
}

terraform {
  backend "gcs" {
    bucket  = "gitlab-analysis-data-terraform-state"
    prefix  = "data-ops-google_dataproc_metastore_service/state"
  }
}

resource "google_dataproc_metastore_service" "sds-data-ops" {
  provider = google-beta
  project = "gitlab-analysis"
  service_id = "sds-data-ops"
  location   = "us-west1"
  port       = 9080
  tier       = "DEVELOPER"

  maintenance_window {
    hour_of_day = 2
    day_of_week = "SUNDAY"
  }

  hive_metastore_config {
    version = "3.1.2"
    endpoint_protocol="THRIFT"
  }
  network_config {
    consumers{
      subnetwork = "projects/gitlab-analysis/regions/us-west1/subnetworks/279143275596279027"
    }
  }
}